(** * The Delay Monad *)

(** Contents:
  - The [delay] monad
  - Predicates for terminating computations: [terminates_in] and [terminates_with]
  - Ordering and equivalence: [refines] and [coterminate]
  *)

From Coq Require Import Program.Basics.

Set Primitive Projections.
Set Implicit Arguments.
Set Contextual Implicit.

(** * [delay] *)

Variant step (A B : Type) : Type :=
| Stop : A -> step A B
| Step : B -> step A B
.

CoInductive delay (A : Type) : Type := Delay
  { force : step A (delay A) }.

(** * Operations *)

Module Delay.

Definition ret {A} (a : A) : delay A :=
  Delay (Stop a).

Definition subst {A B} (f : A -> delay B) : delay A -> delay B :=
  cofix subst_ (u : delay A) : delay B := Delay
    match force u with
    | Stop x => force (f x)
    | Step u => Step (subst_ u)
    end.

Definition bind {A B} (u : delay A) (f : A -> delay B) : delay B :=
  subst f u.

Definition iter {A B} (f : A -> delay (B + A)) : A -> delay B :=
  cofix iter_ (x : A) :=
    Delay.bind (f x) (fun xy => Delay
      match xy with
      | inl y => Stop y
      | inr x => Step (iter_ x)
      end).

End Delay.

(** * Relations *)

(** [terminates_in n x u]: the computation [u] terminates in [n] steps
    with result [x]. *)
Fixpoint terminates_in {A} (n : nat) (x : A) (u : delay A) : Prop :=
  match n, force u with
  | O, Stop x' => x = x'
  | S n, Step u => terminates_in n x u
  | _, _ => False
  end.

(** [terminates_with P u]: the computation [u] with result satisfying the
 * post-condition [P]. *)
Definition terminates_with {A} (P : A -> Prop) (u : delay A) : Prop :=
  exists n x, terminates_in n x u /\ P x.

(* [refines u v]: u terminates implies v terminates *)
Definition refines {A B} (R : A -> B -> Prop) (u : delay A) (v : delay B) : Prop :=
  forall x, terminates_with (eq x) u -> terminates_with (R x) v.

(* [coterminate u v]: u terminates iff v terminates *)
Definition coterminate {A B} (R : A -> B -> Prop) (u : delay A) (v : delay B) : Prop :=
  refines R u v /\ refines (flip R) v u.
