From Coq Require Import Program.Basics Setoid Morphisms Morphisms_Prop.
From Delay Require Import Definitions.

Set Primitive Projections.
Set Implicit Arguments.
Set Contextual Implicit.

Lemma exists_f {A} {P : A -> Prop} {B} (f : B -> A) : ex (fun y => P (f y)) -> ex P.
Proof.
  intros [y Hy]; exact (ex_intro _ (f y) Hy).
Qed.

#[local]
Notation pr := (pointwise_relation _).

(** * [terminates_in] *)

Lemma unfold_terminates_in {A} (n : nat) (x : A) (u : delay A)
  : terminates_in n x u
  = match n, force u with
    | O, Stop x' => x = x'
    | S n, Step u => terminates_in n x u
    | _, _ => False
    end.
Proof. destruct n; reflexivity. Qed.

(** * [terminates_with] *)

#[global] Instance impl_terminates_with {A} : Proper (pr impl ==> pr impl) (@terminates_with A).
Proof. intros P Q HP u (n & x & Hn & Hx). exists n, x. exact (conj Hn (HP x Hx)). Qed.

#[global] Instance impl_terminates_with' {A} : Proper (pr impl ==> eq ==> impl) (@terminates_with A).
Proof.
  intros P Q HP u _ <-; apply impl_terminates_with; auto.
Qed.

Lemma terminates_with_split {A} (P : A -> Prop) (u : delay A)
  : terminates_with P u -> exists x, terminates_with (eq x) u /\ P x.
Proof.
  intros (n & x & Hn & Hx); exists x; split; [ | auto ].
  exists n, x; auto.
Qed.

(** * [refines] *)

(* Preorder *)

#[global] Instance Reflexive_refines {A} (R : A -> A -> Prop) {Refl : Reflexive R}
  : Reflexive (refines R).
Proof.
  intros u x. apply impl_terminates_with. intros x'; red; apply eq_subrelation, Refl.
Qed.

Definition trans_refines {A B C} (RAB : A -> B -> Prop) (RBC : B -> C -> Prop) (RAC : A -> C -> Prop)
    (HR : forall x y z, RAB x y -> RBC y z -> RAC x z)
  : forall u v w, refines RAB u v -> refines RBC v w -> refines RAC u w.
Proof.
  intros u v w Huv Hvw x (n & _ & Hn & <-). 
  destruct (Huv x (ex_intro _ n (ex_intro _ x (conj Hn eq_refl)))) as (m & y & Hm & Hy).
  destruct (Hvw y (ex_intro _ m (ex_intro _ y (conj Hm eq_refl)))) as (p & z & Hp & Hz).
  exact (ex_intro _ p (ex_intro _ z (conj Hp (HR _ _ _ Hy Hz)))).
Qed.

#[global] Instance Transitive_refines {A} (R : A -> A -> Prop) {Trans : Transitive R}
  : Transitive (refines R).
Proof.
  red. apply trans_refines. apply transitivity.
Qed.

#[global] Instance subrelation_refines {A}
   : Proper (subrelation ==> subrelation) (refines (A := A)).
Proof.
  intros R R' HR u v. unfold refines.
  change (subrelation R R') with (forall x, pr impl (R x) (R' x)) in HR.
  setoid_rewrite <- HR. auto.
Qed.

(** * [coterminate] **)

(* Equivalence *)

#[global] Instance Reflexive_coterminate {A} (R : A -> A -> Prop) {Refl : Reflexive R}
  : Reflexive (coterminate R).
Proof.
  split; reflexivity.
Qed.

#[global] Instance Symmetric_coterminate {A} (R : A -> A -> Prop) {Sym : Symmetric R}
  : Symmetric (coterminate R).
Proof.
  split.
  - generalize (proj2 H). apply subrelation_refines. apply (subrelation_symmetric R _).
  - generalize (proj1 H). apply subrelation_refines. apply (subrelation_symmetric (flip R) _).
Qed.

#[global] Instance Transitive_coterminate {A} (R : A -> A -> Prop) {Trans : Transitive R}
  : Transitive (coterminate R).
Proof.
  intros ? ? ? H1 H2; split; etransitivity; try apply H1; try apply H2.
Qed.

(** * Monad *)

(** Properties involving [Delay.ret] and [Delay.bind] *)

(** ** Closure of [terminates_in] and [terminates_with] under [bind]. *)

Lemma terminates_bind {A B} (u : delay A) (f : A -> delay B)
  : forall n x, terminates_in n x u -> forall m y, terminates_in m y (f x) ->
    terminates_in (n + m) y (Delay.bind u f).
Proof.
  intros n x; revert u; induction n as [|n IH];
    setoid_rewrite (unfold_terminates_in _ _ (Delay.bind _ _));
    cbn; intros u; destruct (force u); try contradiction.
  - intros [] *. rewrite <- unfold_terminates_in. auto.
  - apply IH.
Qed.

Lemma terminates_with_bind {A B} (u : delay A) (f : A -> delay B)
  : forall P, terminates_with P u -> forall Q, (forall x, P x -> terminates_with Q (f x)) ->
    terminates_with Q (Delay.bind u f).
Proof.
  intros P (n & x & Hn & Hx) Q Hf.
  destruct (Hf x Hx) as (m & y & Hm & Hy).
  exists (n + m), y. split; [ | auto ].
  eauto using terminates_bind.
Qed.

(** ** Inversion of [terminates_in] and [terminates_with] under [bind] *)

Lemma terminates_bind_inv {A B} (u : delay A) (f : A -> delay B)
  : forall n y, terminates_in n y (Delay.bind u f) ->
    exists m p x, terminates_in m x u /\ terminates_in p y (f x) /\ n = m + p.
Proof.
  intros n y; revert u; induction n as [|n IH]; intros u;
    setoid_rewrite (unfold_terminates_in _ _ u);
    cbn; destruct (force u) as [x|x]; try contradiction; intros Hu.
  - exists 0, 0, x; cbn; auto.
  - exists 0, (S n), x; cbn; auto.
  - apply (exists_f S); cbn.
    assert (HH : forall a b c, impl (a = b + c) (S a = S (b + c))).
    { intros; exact (@f_equal _ _ S _ _). }
    setoid_rewrite <- HH. apply IH. auto.
Qed.

Lemma terminates_with_bind_inv {A B} (u : delay A) (f : A -> delay B)
  : forall Q, terminates_with Q (Delay.bind u f) ->
     exists x, terminates_with (eq x) u /\ terminates_with Q (f x).
Proof.
  intros Q (n & y & Hn & Hy). apply terminates_bind_inv in Hn.
  destruct Hn as (m & p & x & Hu & Hf & _).
  exists x. split.
  - exists m, x. auto.
  - exists p, y. auto.
Qed.

(** ** Closure and inversion lemmas for [ret] *)

Lemma terminates_in_ret {A} (x : A) : terminates_in 0 x (Delay.ret x).
Proof. cbn; auto. Qed.

Lemma terminates_in_ret_inv {A} (x : A)
  : forall n x', terminates_in n x' (Delay.ret x) -> 0 = n /\ x = x'.
Proof.
  intros [] x'; cbn; [ auto | contradiction ].
Qed.

Lemma terminates_with_ret {A} (x : A) : forall (P : A -> Prop), P x -> terminates_with P (Delay.ret x).
Proof.
  intros; exists 0, x; auto using terminates_in_ret.
Qed.

Lemma terminates_with_ret_inv {A} (x : A)
  : forall P, terminates_with P (Delay.ret x) -> P x.
Proof.
  intros P (n & x' & Hn & H). apply terminates_in_ret_inv in Hn. destruct Hn as [_ <-]; auto.
Qed.

(** ** Monad laws *)

Lemma ret_bind {A B} (x : A) (f : A -> delay B) : coterminate eq (Delay.bind (Delay.ret x) f) (f x).
Proof.
  split; intros y Hy.
  - apply terminates_with_bind_inv in Hy. destruct Hy as (x' & Hx & Hy).
    apply terminates_with_ret_inv in Hx. destruct Hx. auto.
  - apply terminates_with_bind with (P := eq x).
    + apply terminates_with_ret, eq_refl.
    + intros _ <-; auto. revert Hy; apply impl_terminates_with. do 3 red. auto.
Qed.

Lemma bind_ret {A} (u : delay A) : coterminate eq (Delay.bind u Delay.ret) u.
Proof.
  split; intros y Hy.
  - apply terminates_with_bind_inv in Hy. destruct Hy as (x' & Hx & Hy).
    apply terminates_with_ret_inv in Hy. destruct Hy. auto.
  - apply terminates_with_bind with (P := eq y); [ auto | ].
    intros _ <-; apply terminates_with_ret. red; auto.
Qed.

Lemma bind_bind {A B C} (u : delay A) (f : A -> delay B) (g : B -> delay C)
  : coterminate eq (Delay.bind (Delay.bind u f) g) (Delay.bind u (fun x => Delay.bind (f x) g)).
Proof.
  split; intros c Hc.
  - apply terminates_with_bind_inv in Hc. destruct Hc as (b & Hb & Hc).
    apply terminates_with_bind_inv in Hb. destruct Hb as (a & Ha & Hb).
    apply terminates_with_bind with (P := eq a); [ auto | ]. intros _ <-.
    apply terminates_with_bind with (P := eq b); [ auto | ]. intros _ <-. auto.
  - apply terminates_with_bind_inv in Hc. destruct Hc as (a & Ha & Hc).
    apply terminates_with_bind_inv in Hc. destruct Hc as (b & Hb & Hc).
    assert (E : forall x : C, pr impl (eq x) (flip eq x)).
    { red. red. red. auto. }
    rewrite <- E; clear E.
    apply terminates_with_bind with (P := eq b); [ | intros _ <-; auto ].
    apply terminates_with_bind with (P := eq a); [ auto | intros _ <-; auto ].
Qed.

(** Congruence *)

Lemma refines_bind {A A' RA} (u : delay A) (u' : delay A')
    {B B' RB} (f : A -> delay B) (f' : A' -> delay B')
  : refines RA u u' -> (forall x x', RA x x' -> refines RB (f x) (f' x')) ->
    refines RB (Delay.bind u f) (Delay.bind u' f').
Proof.
  intros Hu Hf y Hy.
  apply terminates_with_bind_inv in Hy. destruct Hy as (x & Hx & Hy).
  apply Hu in Hx. apply terminates_with_split in Hx. destruct Hx as (x' & Hx' & HA).
  apply (Hf _ _ HA) in Hy.
  apply terminates_with_bind with (P := eq x'); [ auto | intros _ <-; auto ].
Qed.

Lemma coterminate_bind {A A' RA} (u : delay A) (u' : delay A')
    {B B' RB} (f : A -> delay B) (f' : A' -> delay B')
  : coterminate RA u u' -> (forall x x', RA x x' -> coterminate RB (f x) (f' x')) ->
    coterminate RB (Delay.bind u f) (Delay.bind u' f').
Proof.
  intros [] Hf. split; eapply refines_bind; eauto; intros; apply Hf; auto.
Qed.
